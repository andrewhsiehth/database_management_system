const http = require('http'); 
const querystring = require('querystring');

const httpGet = (options) => {
    return new Promise((resolve, reject) => {
        const req = http.get(options, (res) => { 
            let rawData = []; 
            res.setEncoding('utf8');
            res.on('data', (chunk) => {
                rawData.push(chunk); 
            }); 
            res.on('end', () => {
                resolve(rawData.join(''));
            }); 
        });
        req.on('error', (err) => {
            reject(err);
        });
        req.end();
    });
}; 

const searchArxiv = async (keyword, field='all', sortBy='relevance', sortOrder='descending', start=0, maxResults=500)  => { 
    // method: [search_query, id_list] 
    // field: [all, ti] 
    // sortBy: [relevance, lastUpdatedDate, submittedDate] 
    // sortOrder: [ascending, descending] 
    const query = querystring.stringify({
        search_query: `${field}:"${keyword}"`, 
        sortBy: sortBy, 
        sortOrder: sortOrder,
        start: start, 
        max_results: maxResults 
    }); 
    try {
        const res = await httpGet({
            hostname: 'export.arxiv.org', 
            path: `/api/query?${query}`
        }); 
        return res; 
    } catch (err) {
        console.log(err, err.stack); 
    }
}; 

const searchArxivId = async (id_list) => {
    const query = querystring.stringify({
        id_list: id_list
    }); 
    try {
        const res = await httpGet({
            hostname: 'export.arxiv.org', 
            path: `/api/query?${query}`
        }); 
        return res; 
    } catch (err) {
        console.log(err, err.stack); 
    }
    
};

const arxivXml2JsonEntries = async (arxivXml) => {
    const Xml2JsonParser = require('xml2js-parser'); 
    const parser = new Xml2JsonParser({
        trim: true, 
        mergeAttrs: true 
    }); 
    try {
        const arxivJson = await parser.parseString(arxivXml); 
        return arxivJson.feed.entry;
    } catch (err) {
        console.log(err, err.stack);
    }
}; 

const searchArxivJson = async (keyword, field='all', sortBy='relevance', sortOrder='descending', start=0, maxResults=500) => {
    try {
        const arxivXml = await searchArxiv(keyword, field, sortBy, sortOrder, start, maxResults);
        const arxivJson = await arxivXml2JsonEntries(arxivXml); 
        return arxivJson; 
    } catch (err) {
        console.log(err, err.stack);
    }
};

const searchArxivIdJson = async (id_list) => {
    try {
        const arxivXml = await searchArxivId(id_list);
        const arxivJson = await arxivXml2JsonEntries(arxivXml); 
        return arxivJson; 
    } catch (err) {
        console.log(err, err.stack);
    }
};

module.exports.searchArxiv = searchArxivJson; 
module.exports.searchArxivId = searchArxivIdJson;  


// const main = async (keyword) => {
//     try {
//         const res = await module.exports.searchArxiv(keyword, field='ti', sortBy='relevance', sortOrder='descending', start=0, maxResults=3); 
//         // const res = await module.exports.searchArxivId(keyword);
//         console.log(res); 
//     } catch (err) {
//         console.log(err);
//     }
// };


// main(process.argv[2]);


// const queryArxiv = async (searchQuery, start, maxResults, sortBy, sortOrder) => {
//     const queryString = querystring.stringify({
//         search_query: searchQuery, 
//         start: start, 
//         max_results: maxResults, 
//         sortBy: sortBy, 
//         sortOrder: sortOrder
//     }); 
//     try {
//         const res = await httpGet({
//             hostname: 'export.arxiv.org', 
//             path: `/api/query?${queryString}`
//         });
//         return res; 
//     } catch (err) {
//         console.log(err, err.stack);
//     }
// };

// const queryArxivTitle = async (searchQuery, start, maxResults, sortBy, sortOrder) => {
//     const queryString = querystring.stringify({
//         search_query: `ti:"${searchQuery}"`, 
//         start: start, 
//         max_results: 1, 
//         sortBy: sortBy, 
//         sortOrder: sortOrder
//     }); 
//     try {
//         const res = await httpGet({
//             hostname: 'export.arxiv.org', 
//             path: `/api/query?${queryString}`
//         });
//         return res; 
//     } catch (err) {
//         console.log(err, err.stack);
//     }
// }; 

// const queryArxivId = async (searchQuery, start, maxResults, sortBy, sortOrder) => {
//     const queryString = querystring.stringify({
//         id_list: `${searchQuery}`, 
//         // start: start, 
//         // max_results: maxResults, 
//         // sortBy: sortBy, 
//         // sortOrder: sortOrder
//     }); 
//     try {
//         const res = await httpGet({
//             hostname: 'export.arxiv.org', 
//             path: `/api/query?${queryString}`
//         });
//         return res; 
//     } catch (err) {
//         console.log(err, err.stack);
//     }
// };


// module.exports.getArxiv = async (searchQuery, start=0, maxResults=50, sortBy='relevance', sortOrder='descending') => {
//     try {
//         const res = await queryArxiv(searchQuery, start, maxResults, sortBy, sortOrder); 
//         return res;
//     } catch (err) {
//         console.log(err);
//     }
// };
// module.exports.getArxivByTitle = async (searchQuery, start=0, maxResults=50, sortBy='relevance', sortOrder='descending') => {
//     try {
//         const res = await queryArxivTitle(searchQuery, start, maxResults, sortBy, sortOrder); 
//         return res;
//     } catch (err) {
//         console.log(err);
//     }
// };
// module.exports.getArxivById = async (searchQuery, start=0, maxResults=50, sortBy='relevance', sortOrder='descending') => {
//     try {
//         const res = await queryArxivId(searchQuery, start, maxResults, sortBy, sortOrder); 
//         return res;
//     } catch (err) {
//         console.log(err);
//     }
// };
// module.exports.getArxivByUrl = async (url, start=0, maxResults=50, sortBy='relevance', sortOrder='descending') => {
//     const path = require('path');
//     const id = path.basename(url);
//     try {
//         const res = await queryArxivId(id, start, maxResults, sortBy, sortOrder); 
//         return res;
//     } catch (err) {
//         console.log(err);
//     }
// };


// sortBy = ['relevance', 'lastUpdatedDate', 'submittedDate'] 
// sortOrder = ['ascending', 'descending']




// module.exports.getArxivByUrl(process.argv[2]);