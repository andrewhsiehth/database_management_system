import queryDB 
import convert2tuple 
import os 
import sys 
import time 

if __name__ == '__main__': 
    xlsx_path = sys.argv[1] 

    connection = queryDB.connectDB(
        host=os.environ['DB_HOST'], 
        user=os.environ['DB_USER'], 
        password=os.environ['DB_PASSWORD'], 
        db='paperDemo',
        charset='utf8mb4',
        use_unicode=True
    ) 

    paper, author, tag = convert2tuple.xlsx2dfs(xlsx_path) 
    queries = [ 
        'INSERT INTO paper VALUES %s' % convert2tuple.paperTuplesStr(paper), 
        'INSERT INTO author VALUES %s' % convert2tuple.authorTuplesStr(author), 
        'INSERT INTO paper_author VALUES %s' % convert2tuple.paper_authorTuplesStr(paper, author), 
        'INSERT INTO paper_tag VALUES %s' % convert2tuple.paper_tagTupleStr(paper, tag) 
        # 'INSERT IGNORE INTO paper VALUES %s' % convert2tuple.paperTuplesStr(paper), 
        # 'INSERT IGNORE INTO author VALUES %s' % convert2tuple.authorTuplesStr(author), 
        # 'INSERT IGNORE INTO paper_author VALUES %s' % convert2tuple.paper_authorTuplesStr(paper, author), 
        # 'INSERT IGNORE INTO paper_tag VALUES %s' % convert2tuple.paper_tagTupleStr(paper, tag) 
    ] 
    while True:
        try:
            queryDB.executeQueries(connection, queries)
            print('[end]') 
            break
        except Exception as e: 
            time.sleep(5) 
            connection = queryDB.connectDB(
                host=os.environ['DB_HOST'], 
                user=os.environ['DB_USER'], 
                password=os.environ['DB_PASSWORD'], 
                db='paperDemo',
                charset='utf8mb4',
                use_unicode=True
            ) 
            print('retry')
            pass 


