import pymysql as sql 

def connectDB(**kwargs): 
    connection = sql.connect(autocommit=False, cursorclass=sql.cursors.DictCursor, **kwargs) 
    return connection 
    
def executeQueries(connection, queries): 
    results = []
    try:
        with connection.cursor() as cursor: 
            for i, query in enumerate(queries): 
                print('query', i)
                cursor.execute(query) 
                connection.commit()
                results = cursor.fetchall()
    except Exception as e:
        # print(type(e), e)  
        raise e 
    finally:
        connection.close() 
    return results 

