import pandas as pd 

def xlsx2dfs(path): 
    df = pd.read_excel(path) 
    df = df.fillna('').astype(str) 
    author = df['author'].str.split('$', expand=True).stack().str.strip().reset_index(level=1, drop=True).rename('authorName') 
    df['paperTitle'] = df['title'].str.strip() 
    df['tag'] = df['tag'].str.strip() 
    df['publishDate'] = df['publish date'].str.strip()
    df['insertDate'] = df['insert date'].str.strip() 
    df['note'] = df['note'].str.strip() 
    df['link'] = df['link'].str.strip() 
    df['abstract'] = df['abstract'].str.strip() 
    df['pdf'] = df['link']   
    paper = df[['paperTitle', 'publishDate', 'insertDate', 'pdf', 'link', 'abstract']] 
    tag = df['tag'] 
    return paper, author, tag  

def paperTuplesStr(paper): 
    return ','.join(paper.apply(lambda x: tuple(x).__str__(), axis=1)) 

def authorTuplesStr(author): 
    return ','.join(author.apply(lambda x: ("(\"%s\")"%x).__str__())) 

def paper_authorTuplesStr(paper, author): 
    paperJoinAuthor = paper.join(author) 
    paper_author = paperJoinAuthor[['paperTitle', 'authorName']]
    return ','.join(paper_author.apply(lambda x: tuple(x).__str__(), axis=1)) 

def paper_tagTupleStr(paper, tag):  
    paper_tag = pd.concat([paper['paperTitle'], tag], axis=1) 
    return ','.join(paper_tag.apply(lambda x: tuple(x).__str__(), axis=1)) 