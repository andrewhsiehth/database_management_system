-- 
CREATE DATABASE paperDB;

-- 
USE paperDB; 

-- 
CREATE TABLE paper (
    paperTitle CHAR(255) NOT NULL, 
    publishDate DATETIME, 
    insertDate DATETIME, 
    pdf CHAR(255), 
    link CHAR(255), 
    abstract TEXT 
) DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
INSERT INTO paper (paperTitle, publishDate, insertDate, pdf, link, abstract) 
VALUES 
('The Paper title', '2018-06-15', '2018-06-16', '/path/to/this/paper.pdf', 'http://arxiv.0000.0000', 'This is the abstract with no more than 65535 characters. If you do not want to crash this FUCKING fragile database.');
-- 
CREATE TABLE author (
    authorName CHAR(255) NOT NULL
) DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
INSERT INTO author (authorName) 
VALUES 
('A. Uthor'), 
('Another. Uthor'); 
-- 
CREATE TABLE paper_author (
    paperTitle CHAR(255) NOT NULL, 
    authorName CHAR(255) NOT NULL
) DEFAULT CHARACTER SET utf8 COLLATE utf8_bin; 
INSERT INTO paper_author (paperTitle, authorName) 
VALUES 
('The Paper Title', 'A. Uthor'), 
('The Paper Title', 'Another. Uthor');
-- 
CREATE TABLE user_ (
    userName CHAR(255) NOT NULL,
    userID INT
) DEFAULT CHARACTER SET utf8 COLLATE utf8_bin; 
INSERT INTO user_ (userName, userID) 
VALUES 
('UserOne', 1), 
('UserTwo', 2), 
('UserThree', 3); 
-- 
CREATE TABLE user_paper (
    userName CHAR(255) NOT NULL, 
    paperTitle CHAR(255) NOT NULL, 
    rating FLOAT, 
    note TEXT   
) DEFAULT CHARACTER SET utf8 COLLATE utf8_bin; 
INSERT INTO user_paper(userName, paperTitle, rating, note) 
VALUES 
('UserOne', 'The Paper Title', 0.87, 'Some important notes here'); 
-- 
CREATE TABLE paper_tag (
    paperTitle CHAR(255) NOT NULL, 
    tag CHAR(255) NOT NULL 
) DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
INSERT INTO paper_tag (paperTitle, tag) 
VALUES 
('The Paper Title', 'papertag'), 
('The Paper Title', 'papertag2'); 

