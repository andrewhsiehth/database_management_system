import urllib 
import json 

def request(url, dictData): 
    req = urllib.request.Request(url=url) 
    data = json.dumps(dictData).encode('utf-8') 
    req.add_header('Content-Type', 'application/json; charset=utf-8') 
    req.add_header('Content-Length', len(data)) 
    res = urllib.request.urlopen(req, data)
    return res 