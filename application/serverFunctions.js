const mysql = require('mysql'); 

const queryDatabase = (query, dbName) => {
    const connection = mysql.createConnection({
        host: process.env.DB_HOST, 
        user: process.env.DB_USER, 
        password: process.env.DB_PASSWORD, 
        database: dbName
    }); 
    connection.connect((err) => {
        console.log('[error connecting]', err, err.stack);
    }); 
    connection.query(query, (err, results, fields) => {
        if (err) {
            console.log(err, err.stack);
        } else {
            console.log('[fields]', fields); 
            console.log('[results]', results); 
            connection.end(); 
            return {
                fields: fields, 
                results: results
            };
        }
    });
};

const insertOneQuery = (tableName, valueTuples) => {
    return `INSERT INTO ${tableName} VALUES ${valueTuples};`; 
}; 

const concatenateQueries = (queries) => {
    return queries.join('');
}; 


module.exports.queryDatabase = queryDatabase; 

