const convertFields = (paper) => {
    paper['authorName'] = paper.author.map(x => x.name[0]);  
    paper['link'] = paper.id[0]; 
    paper['paperTitle'] = paper.title[0]; 
    paper['publishDate'] = paper.published[0]; 
    paper['abstract'] = paper.summary[0]; 
    paper['insertDate'] = '2018-06-18';
    return paper; 
};

const paper2paperTuple = (paper) => { 
    return `('${paper.paperTitle}', '${paper.publishDate}', '${paper.insertDate}', '${paper.pdf}', '${paper.link}', '${paper.abstract}')`;
}; 

const paper2authorTuple = (paper) => {
    return `${paper.authorName.map(x => `('${x}')`).join(',')}`;
};

const paper2paper_authorTuple = (paper) => {
    return `${paper.authorName.map(x => `('${paper.paperTitle}', '${x}')`).join(',')}`; 
};

const paper2paper_tagTuple = (paper, tag) => {
    return `('${paper.paperTitle}', '${tag}')`;
}; 

