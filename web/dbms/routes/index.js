var express = require('express');
var router = express.Router();

const mysql = require('mysql'); 

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Paper2Vector' });
});


router.post('/', (req, res, next) => { 
  console.log('server req:', req.body);
  const connection = mysql.createConnection({
    host: process.env.DB_HOST, 
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: 'paperDB'
  }); 
  connection.connect((err) => {
    if (err) { 
      console.log('[error connecting]:', err.stack);
      throw err; 
    }
  }); 
  connection.query(req.body.query, (err, results, fields) => {
    if (err) {
      throw err; 
    } else {
      console.log('fields:', fields); 
      console.log('results:', results);
      connection.end(); 
      res.json({ 
        fields: fields, 
        results: results
      });
    }
  }); 
});

module.exports = router;
