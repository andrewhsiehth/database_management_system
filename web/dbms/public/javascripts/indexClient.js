$(function() { 
    const makeTableHeader = (fields) => {
        const table = $('#query-results'); 
        const thead = $('<thead>');
        const tr = $('<tr>');    
        table.append(thead); 
        thead.append(tr);    
        for (const field of fields) { 
            const th = $('<th>').attr('scope', 'col').text(field.name);
            tr.append(th);
            console.log(th.val());
            console.log(field.name);
        }
    }; 
    const makeTableRows = (fields, results) => { 
        const table = $('#query-results'); 
        const tbody = $('<tbody>');
        table.append(tbody); 
        const fieldOrder = fields.map(x => x.name); 
        for (const row of results) {
            const tr = $('<tr>'); 
            tbody.append(tr);
            for (const field of fieldOrder) {
                const td = $('<td>').text(row[field]); 
                tr.append(td); 
            } 
        }
    };
    console.log(window.location.href);
    $('#submit-form').submit((event) => { 
        console.log(event); 
        event.preventDefault();
        const content = $('#search').val(); 
        const req = {
            query: content 
        };
        console.log(content); 
        if (!$.trim(content)) {
            return false; 
        } else { 
            $.post(window.location.href, req, (res) => { 
                console.log('client req:', req);
                console.log('client res:', res); 
                makeTableHeader(res.fields); 
                makeTableRows(res.fields, res.results);
            });
            $('#search').val('');
        }
    });
})